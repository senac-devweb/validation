Vue.use(vuelidate.default)

new Vue({
  el: '#app',
  data: {
    form: {
      name: null,
      age: null,
      email: null,
      cep: null
    }
  },

  validations: {
    form: {
      name: {
        required: validators.required,
        minLength: validators.minLength(4)
      },

      age: {
        required: validators.required,
        integer: validators.integer,
        minValue: validators.minValue(18)
      },

      email: {
        required: validators.required,
        email: validators.email
      },

      cep: {
        cepValidation: value => /\d{5}\-\d{3}/.test(value)
      }
      
    }
  },
  watch: {
    'form.cep': function (val, oldVal) {
      console.log(/\d{5}\-\d{3}/.test(val));
      
    },
  },

  methods: {

    setSuccess(field){
      return !field.$invalid && field.$model
    },
    setError(field){
      return field.$error
    },

    submitForm() {
      this.$v.form.$touch()

      if (this.$v.form.name.$invalid) {
        console.log('error')
      }else{
        console.log('success')
      }
    }

  }
})